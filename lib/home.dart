import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:tes_twistcode/m_data.dart';
import 'package:tes_twistcode/part.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  double sizeIcon = 18, sizeHalal = 30;

  double widthData;
  bool get;
  List<Data> listdataFix;
  // untuk ambil data
  getData() async {
    setState(() {
      jumlah = 0;
      get = false;
    });
    await http.post(Uri.parse(urlData)).then(
      (http.Response response) {
        // inisial awal
        setState(() {
          sumHarga = 0;
          listdataFix = [];
          listdataCart = [];
          listidCart = [];
          get = true;
        });
        // data dummy untuk keperluan ngecek perhitungan di halaman cart
        listdataFix.add(dummy);
        listdataFix.add(dummy2);

        // response berhasil
        if (response.statusCode == 200) {
          alert(context,
              title: 'Notification',
              content: 'Berhasil ambil data',
              ok: () => Navigator.pop(context));

          var temp = jsonDecode(response.body); // rubah string to list
          for (var i = 0; i < temp.length; i++) {
            var temp2 = temp[i];
            var data = Data(
              id: temp2['id'],
              title: temp2['title'],
              desc: temp2['description'],
              weight: (temp2['weight'] == "") ? '0' : temp2['weight'],
              price: temp2['price'],
              condition: (temp2['condition_of_item']['name'] != "Baru")
                  ? "Bekas"
                  : temp2['condition_of_item']['name'],
              // image:temp2['default_photo']['img_path'],
              image: temp2['category']['default_photo']['img_path'],
              place:
                  (temp2['location_name'] == "") ? "-" : temp2['location_name'],
              owner: temp2['added_user_name'],
              stock: (temp2['stock'] == "") ? '0' : temp2['stock'],
              count: 0,
            );
            listdataFix.add(data);
          }
        } else {
          // response gagal
          alert(context,
              title: 'Notification',
              content: 'Gagal ambil data',
              ok: () => Navigator.pop(context));
        }
      },
    );
  }

  addCart(Data data) {
    if (data.stock == '0') {
      alert(context,
          title: 'Product Habis',
          content: 'Mohon maaf product sudah habis',
          ok: () => Navigator.pop(context));
    } else {
      setState(() {
        data.count++;
        jumlah++;
        sumHarga += int.parse(data.price);
      });
      if (listidCart.contains(data.id)) {
        int urutan = listidCart.indexWhere((element) => element == data.id);
        listdataCart[urutan] = data;
      } else {
        listidCart.add(data.id);
        listdataCart.add(data);
      }
    }
  }

  contData(Data data) {
    return GestureDetector(
      onTap: () {
        print('klik');
      },
      child: Card(
        margin: EdgeInsets.all(elev),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        elevation: elev,
        child: Container(
          width: widthData,
          child: Column(children: [
            Image.network(urlImg + data.image, fit: BoxFit.cover),
            // Image.asset(data.image, fit: BoxFit.cover),
            Container(
              padding: EdgeInsets.all(jarak),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // desc
                    Text(
                      // data.desc + ' = ' + data.count.toString(),
                      data.desc,
                      overflow: TextOverflow.ellipsis,
                      style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                    ),
                    // harga
                    Text(
                      formatCurrency.format(int.parse(data.price)),
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.orange, fontSize: 15),
                    ),
                    // lokasi
                    Row(children: [
                      Icon(Icons.location_on,
                          size: sizeIcon, color: Colors.grey),
                      Container(
                          width: widthData - sizeIcon - sizeHalal - (jarak * 2),
                          child: Text(
                            data.place,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 14),
                          )),
                    ]),
                    Row(children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // pemilik
                          Row(children: [
                            Icon(Icons.person,
                                size: sizeIcon, color: Colors.grey),
                            Container(
                                width: widthData -
                                    sizeIcon -
                                    sizeHalal -
                                    (jarak * 2),
                                child: Text(
                                  data.owner,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(fontSize: 14),
                                )),
                          ]),
                          SizedBox(height: jarak / 2),
                          // tombol beli
                          tombol(
                              (data.stock == '0') ? Colors.red : Colors.blue,
                              (data.stock == '0')
                                  ? 'Out of Stock'
                                  : 'Ready Stock',
                              () => addCart(data)),
                        ],
                      ),
                      // gambar halal
                      Image.asset('assets/halal.png', width: sizeHalal),
                    ])
                  ]),
            ),
          ]),
        ),
      ),
    );
  }

  setFullLayar(BuildContext context) {
    setState(() {
      fullwidth = MediaQuery.of(context).size.width;
      fullheight = MediaQuery.of(context).size.height;
      widthData = (fullwidth - (3 * jarak) - (2 * elev)) / 2;
    });
  }

  @override
  initState() {
    super.initState();

    getData();
  }

  FutureOr _goBack(value) {
    setState(() {
      jumlah = jumlah;
      listdataFix = listdataFix;
    });
  }

  _decide() {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Shopping Cart'),
          actions: [IconCart(num: jumlah.toString(), goback: _goBack)],
        ),
        body: Center(
          child: Stack(children: [
            (get)
                ?
                // list data produk
                SingleChildScrollView(
                    child: Container(
                        padding: EdgeInsets.all(jarak),
                        child: Wrap(
                          children: listdataFix
                              .map((item) => contData(item))
                              .toList()
                              .cast<Widget>(),
                        )),
                  )
                : load,

            // tombol category dan filter
            Align(
              alignment: Alignment(0, 0.95),
              child: Material(
                elevation: elev,
                color: Colors.white,
                borderRadius: BorderRadius.circular(rad),
                child: Container(
                  width: (fullwidth / 2.5),
                  height: 60,
                  padding: EdgeInsets.all(jarak),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(children: [
                        Icon(Icons.format_list_bulleted_rounded),
                        Text('Category')
                      ]),
                      Column(children: [
                        Icon(Icons.filter_list),
                        Text('Filter'),
                      ]),
                      // GestureDetector(
                      //   onTap: () => getData(),
                      //   child: Column(children: [
                      //     Icon(Icons.refresh),
                      //     Text('Refresh'),
                      //   ]),
                      // ),
                    ],
                  ),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    setFullLayar(context);
    // return (get) ? _decide() : loading();
    return _decide();
  }
}
