class Data {
  Data(
      {this.id,
      this.title,
      this.desc,
      this.price,
      this.place,
      this.owner,
      this.stock,
      this.count,
      this.image,
      this.weight,
      this.condition});
  String id, title, desc, price, place, owner, stock, image, weight, condition;
  int count;
}
