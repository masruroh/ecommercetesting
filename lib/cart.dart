import 'package:flutter/material.dart';
import 'package:tes_twistcode/m_data.dart';
import 'package:tes_twistcode/part.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  tombolPlusMin({Function onTap, Icon icon, Color color}) {
    return Material(
      elevation: elev,
      borderRadius: BorderRadius.circular(5),
      color: color.withOpacity(0.8),
      child: GestureDetector(onTap: onTap, child: Container(child: icon)),
    );
  }

  contData(Data data, int urut) {
    return Container(
      margin: EdgeInsets.only(bottom: jarak),
      child: Material(
        borderRadius: BorderRadius.circular(rad),
        elevation: elev,
        child: Container(
          padding: EdgeInsets.all(jarak),
          child: Row(children: [
            // Image.asset(data.image, fit: BoxFit.cover, width: 75, height: 75),
            ClipRRect(
              borderRadius: BorderRadius.circular(rad * 1.5),
              child: Image.network(urlImg + data.image,
                  fit: BoxFit.cover, width: 75, height: 75),
            ),
            SizedBox(width: jarak),
            Flexible(
                child: Container(
                    width: double.infinity,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          data.desc,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: jarak / 2),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      // formatCurrency.format(
                                      //     int.parse(data.price) * data.count),
                                      formatCurrency
                                          .format(int.parse(data.price)),
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: Colors.orange, fontSize: 15),
                                    ),
                                    SizedBox(height: jarak / 2),
                                    Text(
                                      '(' + data.condition + ')',
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 14),
                                    ),
                                  ]),
                            ),
                            Container(
                                child: Column(
                              children: [
                                Row(children: [
                                  tombolPlusMin(
                                      icon: Icon(Icons.remove,
                                          color: Colors.white),
                                      onTap: () {
                                        if (data.count == 1) {
                                          alert(
                                            context,
                                            title: 'Konfirmasi',
                                            content:
                                                'Apa anda mau keluarkan produk ini dari keranjang belanja?',
                                            ok: () {
                                              listidCart.removeAt(urut);
                                              listdataCart.removeAt(urut);
                                              setState(() {
                                                data.count--;
                                                jumlah--;
                                                sumHarga -=
                                                    int.parse(data.price);
                                              });
                                              Navigator.pop(context);
                                            },
                                          );
                                        } else {
                                          setState(() {
                                            data.count--;
                                            jumlah--;
                                            sumHarga -= int.parse(data.price);
                                          });
                                        }
                                      },
                                      color: Colors.red),
                                  Container(
                                      margin: EdgeInsets.only(
                                          right: jarak, left: jarak),
                                      child: Text(
                                        data.count.toString(),
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(fontSize: 14),
                                      )),
                                  tombolPlusMin(
                                      icon:
                                          Icon(Icons.add, color: Colors.white),
                                      onTap: () {
                                        setState(() {
                                          data.count++;
                                          jumlah++;
                                          sumHarga += int.parse(data.price);
                                        });
                                      },
                                      color: Colors.blue),
                                ]),
                                SizedBox(height: jarak / 2),
                                Text(
                                  (int.parse(data.weight) * data.count)
                                          .toString() +
                                      ' kg',
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(fontSize: 14),
                                ),
                              ],
                            )),
                          ],
                        ),
                      ],
                    )))
          ]),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Keranjang Belanja'),
          actions: [
            // IconButton(
            //   onPressed: () => setState(() {
            //     for (var i = 0; i < listdata.length; i++) {
            //       Data temp = listdata[i];
            //       setState(() => temp.count = 0);
            //     }
            //     jumlah = 0;
            //     listdata = [];
            //     listidCart = [];
            //   }),
            //   icon: Icon(Icons.delete),
            // )
          ],
        ),
        body: Container(
          child: Stack(
            children: [
              Container(
                padding: EdgeInsets.all(jarak),
                child: Container(
                  padding: EdgeInsets.only(bottom: 70),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Produk yang akan anda pesan',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                      SizedBox(height: jarak),
                      Flexible(
                          child: (listdataCart.length == 0)
                              ? Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/empty-cart.png',
                                        height: 100,
                                      ),
                                      SizedBox(height: jarak),
                                      Text(
                                        'Keranjang belanja anda kosong :(',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      )
                                    ],
                                  ),
                                )
                              : ListView.builder(
                                  itemCount: listdataCart.length,
                                  itemBuilder: (context, i) {
                                    return contData(listdataCart[i], i);
                                  },
                                )),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment(0, 1),
                child: Container(
                    height: 70,
                    padding: EdgeInsets.all(jarak),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(rad * 3),
                        topRight: Radius.circular(rad * 3),
                      ),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 10,
                          color: Colors.grey[350],
                          offset: Offset(0.0, -2.0),
                        )
                      ],
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Total harga',
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: Colors.blue,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17),
                              ),
                              Text(
                                formatCurrency.format(sumHarga),
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: Colors.orange,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15),
                              ),
                            ]),
                        MaterialButton(
                          onPressed: () {},
                          child: Text('Order'),
                          color: Colors.orange,
                          textColor: Colors.white,
                          splashColor: Colors.orange,
                        )
                        // tombol(Colors.orange, 'Order', () {}),
                      ],
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
