import 'dart:async';

import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:tes_twistcode/cart.dart';
import 'package:tes_twistcode/m_data.dart';

final formatCurrency = new NumberFormat.simpleCurrency(locale: 'id_ID');

double jarak = 10, elev = 5, rad = 10;
List<Data> listdataCart;
List<String> listidCart;
String urlImg = 'https://ranting.twisdev.com/uploads/';
String urlData =
    'https://ranting.twisdev.com/index.php/rest/items/search/api_key/teampsisthebest/';
double fullwidth, fullheight;
int jumlah, sumHarga;
Data dummy = Data(
    title: 'Sayuran',
    id: 'Sayuran bayam',
    desc: 'Sayuran bayam',
    weight: '10',
    price: '50000',
    condition: 'Baru',
    image: 'Buah_Sayur.png',
    place: 'Kota Surabaya',
    owner: 'Dummy 1 By Izza',
    stock: '100',
    count: 0);
Data dummy2 = Data(
    title: 'Sayuran',
    id: 'Sayuran wortel ',
    desc: 'Sayuran wortel ',
    weight: '20',
    price: '25000',
    condition: 'Baru',
    image: 'Buah_Sayur.png',
    place: 'Kota Mojokerto',
    owner: 'Dummy 2 By Izza',
    stock: '100',
    count: 0);

tombol(Color color, String title, Function ontap) {
  return GestureDetector(
    onTap: ontap,
    child: Container(
        padding: EdgeInsets.fromLTRB(jarak, jarak / 2, jarak, jarak / 2),
        decoration: BoxDecoration(
            color: color, borderRadius: BorderRadius.circular(rad)),
        child: Text(title, style: TextStyle(color: Colors.white))),
  );
}
// untuk alert

alert(BuildContext context, {String title, String content, Function ok}) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Center(child: Text(title)),
        actions: [
          MaterialButton(child: Text('OK'), onPressed: ok, color: Colors.blue)
        ],
        content: SingleChildScrollView(child: Text(content)),
      );
    },
  );
}

var load = Center(
  child: Container(
    width: 70,
    child: Image.asset('assets/loading.gif', fit: BoxFit.cover),
  ),
);
loading() {
  return SafeArea(child: Scaffold(body: load));
}

class IconCart extends StatefulWidget {
  IconCart({this.num, this.data, this.goback});
  final String num;
  final Data data;
  final FutureOr goback;
  @override
  _IconCartState createState() => _IconCartState();
}

class _IconCartState extends State<IconCart> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      child: Stack(
        children: [
          Row(
            children: [
              SizedBox(width: 18),
              Container(
                  padding: EdgeInsets.all(3),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.red,
                  ),
                  child: Text(widget.num)),
            ],
          ),
          Center(
              child: GestureDetector(
            onTap: () {
              Navigator.push(context,
                      MaterialPageRoute(builder: (context) => CartPage()))
                  .then(widget.goback);
            },
            child: Icon(Icons.shopping_cart_rounded),
          )),
        ],
      ),
    );
  }
}
