# ECommerceTesting

ECommerceTesting adalah aplikasi jual beli

## Getting Started

### Tampilan loading page
![get data](assets/blob/loading.jpeg )

### Tampilan berhasil ambil data dari server
![get data](assets/blob/berhasil_get_data.jpeg )

### Tampilan home
![get data](assets/blob/home_page.jpeg )

### Tampilan jika stock produk habis
![get data](assets/blob/stok_habis.jpeg )

### Tampilan keranjang belanja
![get data](assets/blob/cart_page.jpeg )

### Tampilan keranjang belanja kosong
![get data](assets/blob/cart_empty.jpeg )

### Tampilan konfirmasi hapus produk dari keranjang belanja
![get data](assets/blob/cart_page_confirm.jpeg )
